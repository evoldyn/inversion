#!/usr/bin/perl



$PREFIX = $ARGV[0];


open(MUT, "$PREFIX") or die;
open (OUT_DEL_INV, ">inversion_del.txt");
open (OUT_DEL_COL, ">collinear_del.txt");
open (OUT_LA_INV, ">inversion_LA.txt");
open (OUT_LA_COL, ">collinear_LA.txt");

@inv_del = ();
@col_del = ();
@inv_la = ();
@col_la = ();

while(<MUT>) {
    chomp;
		@in = split(/\s+/, $_);
		if ( 50000	<= $in[6] && $in[6] <= 79999  &&	$in[5] eq 'm5' ) {
			push @inv_del, "$_\n";
    }  elsif ( 50000 > $in[6] | $in[6] > 79999 &&	$in[5] eq 'm5' ) {
			push @col_del, "$_\n";
	} elsif ( 50000	<= $in[6] && $in[6] <= 79999  &&	$in[5] =~ /m3|m4/ ) {
			push @inv_la, "$_\n";
	}
	elsif ( 50000	> $in[6]  | $in[6]> 79999 &&	$in[5] =~ /m3|m4/ ) {
			push @col_la, "$_\n";
	}
}
	
	print OUT_DEL_INV "@inv_del";
	print OUT_DEL_COL "@col_del";
	print OUT_LA_INV "@inv_la";
	print OUT_LA_COL "@col_la";
