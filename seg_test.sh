#!/bin/bash


#------------------------------------------------------------------------------------------
#--- Bash script to replicate the potential clustering, through a decreased in recombination rate instead of an inversion.---
#------------------------------------------------------------------------------------------

# Define the population size N, the recombination rate, rho, according to Slim (gene conversion + crossing over) the ratio, gc, gene conversion / (gene conversion + crossing over), and the selection of the heterozygote, sigma.  
N=2500
rho=0.00000108
gc=0.833
sigma=1.03

# do ten replciates
for ((j=1; j<=10; j+=1))
	do  

	# choose a random seed using python
	Seed=$(python -c 'import random as R; print(R.randint(1, 2**32-1))')
	# call slim
	slim -seed $Seed -d rec=$rho -d gen_conv=$gc -d s_het=$sigma -d Npop=$N ./segregation_only.slim

	echo "$Seed, $N, $rho, $gc, $sigma, $j" > temp.txt
	
	# call R to calculate the potential clusters
	R CMD BATCH calculate_cluster.R
	
	# provide unique names for the output files, based on the parameter used.
	mv fitness_pop.txt fitness_pop_N${N}_rec${rho}_GC${gc}_${j}.txt
	mv seg_full_pop.txt seg_full_pop_N${N}_rec${rho}_GC${gc}_${j}.txt
	mv hap_matrix.csv hap_matrix_N${N}_rec${rho}_GC${gc}_${j}.csv
	mv struct.png struct_N${N}_rec${rho}_GC${gc}_${j}.png
	mv Cluster_number.pdf Cluster_number_N${N}_rec${rho}_GC${gc}_${j}.pdf
	
done

