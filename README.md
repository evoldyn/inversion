For this project, we used Slim version 2.6 (Haller) available here (https://messerlab.org/slim/).

# Burn in
The burn in can be recreated by running the [**slim_burn_in.slim**](https://gitlab.com/evoldyn/inversion/blob/master/slim_burn_in.slim) file. We also provide the output of the run that we used for the rest of the analyses, [**burn_in.txt**](https://gitlab.com/evoldyn/inversion/blob/master/burn_in.txt).

# Simulations
## Main case
The simulations without GC were run using the [**inversion_forall_nogc.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_nogc.slim) file. An example of how to call the file is provided in [**for_each_indv_nogc.sh**](https://gitlab.com/evoldyn/inversion/blob/master/for_each_indv_nogc.sh).

The simulations with GC were run using the [**inversion_forall_gc.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_gc.slim) file. An example of how to call the file is provided in [**for_each_indv_gc.sh**](https://gitlab.com/evoldyn/inversion/blob/master/for_each_indv_gc.sh).

Change in the selection coefficient, s<sub>HET</sub>, can be done directly from the bash file.

## Small populations
The simulations for small populations were run using the [**inversion_forall_nogc_500.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_nogc_500.slim) file for the case without GC and the [**inversion_forall_gc_500.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_gc_500.slim) file for the case with GC. An example of how to call the file is provided in [**for_each_indv_nogc_small.sh**](https://gitlab.com/evoldyn/inversion/blob/master/for_each_indv_nogc_small.sh) and  [**for_each_indv_gc_small.sh**](https://gitlab.com/evoldyn/inversion/blob/master/for_each_indv_gc_small.sh).

## Extra time

For specific runs that remained polymorphic after 500 000 generations, we ran the simulation for an additional million generations. Slim code is provided in the [**inversion_forall_nogc_long.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_nogc_long.slim) file with its corresponding bash script [**for_each_indv_nogc_long.sh**](https://gitlab.com/evoldyn/inversion/blob/master/for_each_indv_nogc_long.sh) in the absence of GC. Similarly, in presence of GC, the slim code is provided in the [**inversion_forall_gc_long.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_gc_long.slim) file with its corresponding bash script [**for_each_indv_gc_long.sh**](https://gitlab.com/evoldyn/inversion/blob/master/for_each_indv_gc_long.sh)

## Larger inversions
The simulations for small populations were run using the [**inversion_forall_nogc_larger_inversion.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_nogc_larger_inversion.slim) file for the case without GC and the [**inversion_forall_gc_larger_inversion.slim**](https://gitlab.com/evoldyn/inversion/blob/master/inversion_forall_gc_larger_inversion.slim) file for the case with GC. The bash script is similar to the one for the default case.

## Replicating haplotype structuring

To replicate some of our results (haplotype structuring) and demonstrate the effect of the reduction in recombination rate, we ran extra simulations, with the code given in [**segregation_only.slim**](https://gitlab.com/evoldyn/inversion/blob/master/segregation_only.slim). An example of how to call the file is provided in [**seg_test.sh**](https://gitlab.com/evoldyn/inversion/blob/master/seg_test.sh). To determine whether we observe haplotype structuring in the "simpler case"  we are using R and the main part of the code can be found in [**check_hap_structuring.R**](https://gitlab.com/evoldyn/inversion/blob/master/check_hap_structuring.R) file. This code reads the various fitness files and checks whether fitness is at least bimodal. Outcome corresponds to Fig S4 of the manuscript.

# Seeds
The seeds used for the no GC simulations are provided in the [**master_file_nogc_part1.csv**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_nogc_part1.csv), [**master_file_nogc_part2.csv**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_nogc_part2.csv),  and [**master_file_nogc_part3.csv**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_nogc_part3.csv) files. The following files [**master_file_nogc_improved_all.rar**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_nogc_imprived_all.rar) and [**master_file_gc_improved_all.rar**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_gc_improved_all.rar) contained the processed data from the simulations.

The seeds used for the no GC simulations are provided in the  [**master_file_gc.csv**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_gc_.csv) file.

Additional data relative to the exploration of different s<sub>HET</sub> are available in the following files: [**Shet_MA.csv**](https://gitlab.com/evoldyn/inversion/blob/master/Shet_MA.csv), [**part1.tar.gz**](https://gitlab.com/evoldyn/inversion/blob/master/part1.tar.gz), [**part2.tar.gz**](https://gitlab.com/evoldyn/inversion/blob/master/part2.tar.gz) and [**master_file_nogc_improved_all_1.06shet.csv**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_nogc_improved_all_1.06shet.csv).

Additional data relative to different GC rates are available in [**GC_master_table.csv**](https://gitlab.com/evoldyn/inversion/blob/master/GC_master_table.csv), [**GC_gradient_good_runs.csv**](https://gitlab.com/evoldyn/inversion/blob/master/GC_gradient_good_runs.csv) and [**gc_gradient.csv**](https://gitlab.com/evoldyn/inversion/blob/master/gc_gradient.csv).

Additional data relative to a larger inversion are available in [**master_file_improved_all_big_GC.csv**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_improved_all_big_GC.csv) and [**master_file_improved_all_big_nogc.csv**](https://gitlab.com/evoldyn/inversion/blob/master/master_file_improved_all_big_nogc.csv).


# Analysis

The analysis was conducted in R. The main resuts are presented in an R notebook, [**analysis.Rmd**](https://gitlab.com/evoldyn/inversion/blob/master/analysis.Rmd). The functions themselves are given separately in the  [**slim_analysis_functions_final.R**](https://gitlab.com/evoldyn/inversion/blob/master/slim_analysis_functions_final.R) file. Additional analysis (extra selection coefficients, larger inversions and additional GC rates) are avaialable in the [**Additional_analysis.Rmd**](https://gitlab.com/evoldyn/inversion/blob/master/Additional_analysis.Rmd) file.


