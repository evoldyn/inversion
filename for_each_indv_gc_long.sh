#!/bin/bash

#------------------------------------------------------------------------------------------
#--- Bash script to simulate the fate of an inversion happening in a specific haplotype.---
#------------------------------------------------------------------------------------------


# i stand for the individiual the inversion happens in. Note that Slim follows C nomenclature and the first indiviudal has indice 0.
for ((i=2210; i<=2210; i+=1))
	do  

	# define the name of the folder storing the results.
	PREFIX2="Individual_gc_long_2210_26"

	# check that the fodler does not exsit to avoid erasing already exisitng results.
	if [ -d $PREFIX2 ]; then
		echo "abort simulation: $PREFIX is an existing directory"; 
	    exit 1
	fi


	# create the directory and move there
	mkdir $PREFIX2
	cp burn_in.txt $PREFIX2
	cd $PREFIX2

	# definte the name that all fiel will share. it can be different from the folder name as many files manipulation in R requires specific file name syntax, and soemtimes one need more flexibility for folder names.
	PREFIX="Individual_$i"

	for ((j=26; j<=26; j+=1))
		do  
		
		echo "$PREFIX"
		#Pick the seed of a successful run we want to extend
		Seed=4174818092
		# Run slim
	 	slim -seed $Seed -d indv=$i -d haplo=0 -d s_het=1.03 -d n_sam=100 ./../inversion_forall_gc_long.txt

		# rename the file after their creation. Handling file name is far easier in bash than in Slim, especially if we want importnat parameter values to be included in the name.
		mv del_mutations.txt ${PREFIX}_2_shet1.03_${j}_del_mutations.txt
		mv inversion_data_fm.txt ${PREFIX}_2_shet1.03_${j}_inversion_data_fm.txt
		mv fitness_p1.txt ${PREFIX}_2_shet1.03_${j}_fitness_p1.txt
		mv inversion_counts.txt ${PREFIX}_2_shet1.03_${j}_inversion_counts.txt
		mv final_population.txt ${PREFIX}_2_shet1.03_${j}_final_population.txt



	done

	cd ..

done

