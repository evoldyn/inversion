# create dataframe to store the data
library(ggplot2)
metadata=c()
# the fitness are stored in different folder name "case_1" adnd so on
for (i in c(seq(7),seq(9,20,1))){
  fold=paste("case_",i,sep="")
  setwd(fold)
  # get the files that are in the folder
  fil_lis=list.files(pattern = "fitness")
  for (j in 1:length(fil_lis)){
    values=unlist(strsplit(fil_lis[j],"_"))
    # read the entry for the least generation only
    data_fit=read.table(fil_lis[j],fill=NA,sep=",",skip=598)
    # fous on both homokaryotypes
    alpha_alpha=data_fit[1,data_fit[2,]==2]
    beta_beta=data_fit[1,data_fit[2,]==0]
    # extract parameters values from file name and characterize fitness distribution
    temp=c(as.numeric(sub(".txt","",values[7])),as.numeric(sub("rec","",values[4])),as.numeric(sub("GC","",values[5])),hist(as.numeric(alpha_alpha),breaks=c(0,10^-3,seq(0.1,1.1,0.1)),plot=FALSE)$counts,hist(as.numeric(beta_beta),breaks=c(0,10^-3,seq(0.1,1.1,0.1)),plot=FALSE)$counts)
    metadata=rbind(metadata,temp)
  }
  setwd("../")
}
# gave name to columns
colnames(metadata)=c("rep","rec","GC","adead","a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","bdead","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11")

write.csv(metadata,file = "metadata.csv")

metadata=as.data.frame(metadata)

true_rho=summary_metadata[,1]*(1-summary_metadata[,2])/1.5e-06
true_gc=summary_metadata[,1]*(summary_metadata[,2])/9e-7


# concatenate previous table to have only one entry per parameter combination - with three column: hap strcut happens in both homokaryotypes, in only one or none. 
summary_metadata=unique(metadata[,2:3])
summary_metadata=cbind(summary_metadata,rep(NA,dim(summary_metadata)[1]))
summary_metadata=cbind(summary_metadata,rep(NA,dim(summary_metadata)[1]))
summary_metadata=cbind(summary_metadata,rep(NA,dim(summary_metadata)[1]))
summary_metadata=cbind(summary_metadata,rep(NA,dim(summary_metadata)[1]))
for (i in 1:dim(summary_metadata)[1]){
  y=metadata[metadata[,2]==summary_metadata[i,1]&metadata[,3]==summary_metadata[i,2],]
  summary_metadata[i,3:5]=c(sum(y$adead>0&apply(y[,6:15],1,sum)>0&y$bdead>0&apply(y[,18:27],1,sum)>0),sum((y$adead>0&apply(y[,6:15],1,sum)>0)|(y$bdead>0&apply(y[,18:27],1,sum)>0))-sum(y$adead>0&y$bdead>0),sum(y$adead==0&y$bdead==0))
  
}
matrix_metadata=cbind(cbind(true_rho,true_gc),summary_metadata[,3]+summary_metadata[,4])
colnames(matrix_metadata)[3]="count"

# represent the figure

ggplot(as.data.frame(matrix_metadata),aes(x=true_rho,y=true_gc,col=count>0))+geom_point(shape=15)+xlab("Crossover rate modifier" )+ylab("Gene conversion rate modifier")+theme(text = element_text(size=14))+scale_colour_manual(values=c("black","red"),name="Haplotype structuring")+theme(legend.position = "none")

